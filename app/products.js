const express = require('express');
const multer = require('multer');
const router = express.Router();
const path = require('path');
const nanoid = require('nanoid');
const Product = require('../models/Product');
const User = require('../models/User');
const config = require('../config');
const auth = require('../middleware/auth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {

    router.get('/', (req, res) => {
        const categoryId = req.query.category;

        if(categoryId) {
            Product.find({category: categoryId}).populate('category')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        } else {
            Product.find().populate('category')
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.post('/', [auth, upload.single('image')], async (req, res) => {
        const product = req.body;

        product.user = req.user._id;

        if (req.file) {
            product.image = req.file.filename;
        } else {
            product.image = null;
        }

        const p = new Product(product);

        await p.save();

        return res.send(p);
    });

    router.get('/:id', (req, res) => {
        Product.findOne({_id: req.params.id}).populate('category user')
            .then(result => res.send(result))
            .catch(() => res.sendStatus(500));
    });

    router.delete('/:id', auth,  async (req, res) => {

        Product.deleteOne({_id: req.params.id})
            .then(product => res.send(product))
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;