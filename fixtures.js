const mongoose = require('mongoose');
const config = require('./config');

const Category = require('./models/Category');
const Product = require('./models/Product');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('categories');
        await db.dropCollection('products');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [computerCategory, carCategory, phoneCategory] = await Category.create({
        title: 'Computers'
    }, {
        title: 'Cars'
    }, {
        title: 'Phones'
    });

    const user = await User.create({
        username: 'nurbek19',
        password: '123',
        displayName: 'Nurbek',
        phoneNumber: '+996(702)37-24-47'
    });

    await Product.create({
        title: 'Asus Rog',
        price: 800,
        description: 'Very cool laptop',
        category: computerCategory._id,
        image: 'asus.jpg',
        user: user._id
    }, {
        title: 'Brabus',
        price: 1000000,
        description: 'Some kinda description',
        category: carCategory._id,
        image: 'brabus.jpg',
        user: user._id
    }, {
        title: 'Iphone X',
        price: 1000,
        description: 'Some kinda description',
        category: phoneCategory._id,
        image: 'iphone.jpeg',
        user: user._id
    });

    db.close();
});